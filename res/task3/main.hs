module Demo where

first :: (Eq t1, Num t1, Num t2) => t1 -> t2 -> t2
first n acc | n == 0 = acc
            | otherwise = first (n-1) (acc + 1)


second = first 9 34