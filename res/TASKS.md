# Задания
1. Собственный интеграл<br>
![asd](task1/image.jpg)<br>
Ответ: -4<br>
Латех: `$40 \cdot \int_0^1 \frac{x dx}{(4x-5)^3}$`<br>
Фольфрам: `(integral x/((4x-5)^3) from 0 to 1) * 40`

2. Интерпретировать в голове:
```
def wrapper(f):
    def new_func(x, y):
        print("The", end = ' ')
        f(y, x)
        print("lie")
    return new_func

@wrapper
def func(x = "haven't", y = "existed is"):
    print(f"cake {x} {y}", end = ' ')

func("a", "is")
```
Ответ: The cake is a lie<br>
Отсылать как: The%20cake%20is%20a%20lie
Не забывай, что в url не может быть пробелов

3. Какое значение вернёт функция `second`?
```
module Demo where

first :: (Eq t1, Num t1, Num t2) => t1 -> t2 -> t2
first n acc | n == 0 = acc
            | otherwise = first (n-1) (acc + 1)


second = first 9 34
```
Ответ: 43<br>
Да, это хаскель, но мне как-то всё равно, не зря ж учил

Отослать в на этот сайт гет запросом в формате
site_url:port/answer?task1="task1_answer"&task2="task2_answer"...
В ответ откроется страница с ответом, отсылать лучше из браузера