def wrapper(f):
    def new_func(x, y):
        print("The", end = ' ')
        f(y, x)
        print("lie")
    return new_func

@wrapper
def func(x = "haven't", y = "existed is"):
    print(f"cake {x} {y}", end = ' ')

func("a", "is")

