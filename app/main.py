from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


# class Answer(BaseModel):
#     ans1: str
#     ans2: str
#     ans3: str

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/present",  response_class=HTMLResponse)
async def task(request: Request):
    return templates.TemplateResponse("present.html", {"request": request})
    
@app.get("/task2",  response_class=HTMLResponse)
async def task(request: Request):
    return templates.TemplateResponse("task2.html", {"request": request})

@app.get("/task3",  response_class=HTMLResponse)
async def task(request: Request):
    return templates.TemplateResponse("task3.html", {"request": request})

@app.get("/task4",  response_class=HTMLResponse)
async def task(request: Request):
    return templates.TemplateResponse("task4.html", {"request": request})



@app.get("/asnwer")
async def task(ans1, ans2, ans3):
    ok_count = 0
    # -1 The cake is a lieОтвет: 43<br>
    if ans1 == '-4':
        ok_count+=1
    if ans2 == 'The cake is a lie':
        ok_count+=1
    if ans3 == '43':
        ok_count+=1
    return {'ok_count': ok_count, 'answer': "Пароль" if ok_count == 3 else "Не скажу ответ"}